{
    "id": "4e0d92f9-31eb-4dc8-8621-04847796ca9e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_slash",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 1,
    "bbox_right": 31,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ab761164-f56a-41b6-9531-afd3cb7c693e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e0d92f9-31eb-4dc8-8621-04847796ca9e",
            "compositeImage": {
                "id": "b8fbfc2d-9fda-4980-bd1f-677be0d9efaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab761164-f56a-41b6-9531-afd3cb7c693e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "903b0bd1-495d-441e-a185-bb9e23770ec0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab761164-f56a-41b6-9531-afd3cb7c693e",
                    "LayerId": "a38f5734-4ab6-4925-b96c-8d5d72655097"
                }
            ]
        },
        {
            "id": "5710196e-df17-4a4c-865d-c1b173bab209",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e0d92f9-31eb-4dc8-8621-04847796ca9e",
            "compositeImage": {
                "id": "2dfbc48c-d093-46e3-a231-ecebdfdf69d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5710196e-df17-4a4c-865d-c1b173bab209",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea6ec573-016b-48ba-b07b-d8755810af06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5710196e-df17-4a4c-865d-c1b173bab209",
                    "LayerId": "a38f5734-4ab6-4925-b96c-8d5d72655097"
                }
            ]
        },
        {
            "id": "4374f861-d2fe-4b1e-894a-b5d1d3bcd9a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e0d92f9-31eb-4dc8-8621-04847796ca9e",
            "compositeImage": {
                "id": "b0bb494b-dbae-48e2-9a89-3ddf1a529996",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4374f861-d2fe-4b1e-894a-b5d1d3bcd9a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31b46d65-22b5-464a-aba2-0b004f6c6788",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4374f861-d2fe-4b1e-894a-b5d1d3bcd9a2",
                    "LayerId": "a38f5734-4ab6-4925-b96c-8d5d72655097"
                }
            ]
        },
        {
            "id": "d5b91684-8659-4136-82a5-35d18057033a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e0d92f9-31eb-4dc8-8621-04847796ca9e",
            "compositeImage": {
                "id": "781fdcfc-7044-4e34-8d1e-2fa8910f8687",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5b91684-8659-4136-82a5-35d18057033a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "383a8713-3dfd-4b5b-ab79-43aba5e51341",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5b91684-8659-4136-82a5-35d18057033a",
                    "LayerId": "a38f5734-4ab6-4925-b96c-8d5d72655097"
                }
            ]
        },
        {
            "id": "de80337f-3eba-4789-9f66-395086e318ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e0d92f9-31eb-4dc8-8621-04847796ca9e",
            "compositeImage": {
                "id": "c97bd546-4dd1-43c1-870b-15d5bfd1387a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de80337f-3eba-4789-9f66-395086e318ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d69091a-2fac-4362-8d37-48bb890237dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de80337f-3eba-4789-9f66-395086e318ab",
                    "LayerId": "a38f5734-4ab6-4925-b96c-8d5d72655097"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "a38f5734-4ab6-4925-b96c-8d5d72655097",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4e0d92f9-31eb-4dc8-8621-04847796ca9e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 8
}