{
    "id": "3c2daa77-a169-457b-9194-e571fd9f7575",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_wall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c865e412-2f01-4874-bbf6-ccfdfadcaa14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c2daa77-a169-457b-9194-e571fd9f7575",
            "compositeImage": {
                "id": "ebb889e6-9eee-45ea-b00d-f0939d8e3b46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c865e412-2f01-4874-bbf6-ccfdfadcaa14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d599321-106f-4a56-b2e1-768467f3cc91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c865e412-2f01-4874-bbf6-ccfdfadcaa14",
                    "LayerId": "195a3d77-45f1-4907-9717-85b945969879"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "195a3d77-45f1-4907-9717-85b945969879",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3c2daa77-a169-457b-9194-e571fd9f7575",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}