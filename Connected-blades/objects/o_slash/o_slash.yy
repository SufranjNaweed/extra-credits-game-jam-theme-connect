{
    "id": "c1537953-761d-4e7f-bb6e-2fff2655793c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_slash",
    "eventList": [
        {
            "id": "a6da7674-1c39-4a26-82ef-3adca2425d30",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "c1537953-761d-4e7f-bb6e-2fff2655793c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4e0d92f9-31eb-4dc8-8621-04847796ca9e",
    "visible": true
}