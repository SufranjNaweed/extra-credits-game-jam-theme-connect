
// Get user Input
keyLeft = keyboard_check(vk_left)
keyRight =  keyboard_check(vk_right)
keyJump = keyboard_check_pressed(vk_space)


// if left is pressed move = -1
// if right is pressed move = 1
var move = keyRight - keyLeft;


//horizontal movement
hsp = move  * move_speed;
vsp = vsp + grav;


if (place_meeting(x, y + 1, o_wall) && (keyJump)){
	vsp -=	10;
}


// horizontal collision 
if (place_meeting(x + hsp, y, o_wall)){
	// while not in contact of the wall you can move toward it
	while (!place_meeting(x + sign(hsp), y, o_wall)){
		x += sign(hsp);
		player_dir += sign(hsp)
	}
	
	hsp = 0;
	
}
x = x + hsp;


// vertical collision 
if (place_meeting(x, y + vsp, o_wall)){
	// while not in contact of the wall you can move toward it
	while (!place_meeting(x , y + sign(vsp), o_wall)){
		y += sign(vsp);		
	}
	vsp = 0;
}
y = y + vsp;


